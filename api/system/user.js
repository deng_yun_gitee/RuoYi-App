import upload from '@/utils/upload'
import request from '@/utils/request'

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
	const data = {
		oldPassword,
		newPassword
	}
	return request({
		url: '/system/user/profile/updatePwd',
		method: 'put',
		params: data
	})
}

// 查询用户个人信息
export function getUserProfile() {
	return request({
		url: '/system/user/profile',
		method: 'get'
	})
}

// 修改用户个人信息
export function updateUserProfile(data) {
	return request({
		url: '/system/user/profile',
		method: 'put',
		data: data
	})
}

// 用户头像上传
export function uploadAvatar(data) {
	return upload({
		url: '/system/user/profile/avatar',
		name: data.name,
		filePath: data.filePath
	})
}
export function zdlist(query) {
	return request({
		'url': '/xnth/record/list',
		method: 'get',
		 params: query
	})
}
// 查询重大项目详细
export function zdIndex(recordId) {
	return request({
		'url': '/xnth/record/'+recordId,
		method: 'get',
		
	})
}


//查询
export function listRecord(query) {
  return request({
    url: '/xnth/record/list',
    method: 'get',
    params: query
  })
}
//待办 

export function todoList(query) {
  return request({
    url: '/xnth/supe/todoList',
    method: 'get',
    params: query
  })
}
//已办

export function finishedList(query) {
  return request({
    url: '/xnth/supe/finishedList',
    method: 'get',
    params: query
  })
}

// 新增重大项目 
export function addRecord(data) {
  return request({
    url: '/xnth/record',
    method: 'post',
    data: data
  })
}

// 修改重大项目 //提交
export function updateRecord(data) {
  return request({
    url: '/xnth/record',
    method: 'put',
    data: data
  })
}


// 查询我的收藏列表
export function listReserve(query) {
  return request({
    url: '/xnth/reserve/list',
    method: 'get',
    params: query
  })
}

// 查询我的收藏详细
export function getReserve(id) {
  return request({
    url: '/xnth/reserve/' + id,
    method: 'get'
  })
}

// 新增我的收藏
export function addReserve(data) {
  return request({
    url: '/xnth/reserve',
    method: 'post',
    data: data
  })
}

// 修改我的收藏
export function updateReserve(data) {
  return request({
    url: '/xnth/reserve',
    method: 'put',
    data: data
  })
}

// 删除我的收藏
export function delReserve(id) {
  return request({
    url: '/xnth/reserve/' + id,
    method: 'delete'
  })
}

//  收藏记录数查询
export function reserveCount() {
  return request({
    url: '/xnth/reserve/reserveCount',
    method: 'get'
  })
}
// 查询用户列表
export function listUser(query) {
  return request({
    url: '/system/user/list',
    method: 'get',
    params: query
  })
}
// 新增领导批示 //提交
export function addApproval(data) {
  return request({
    url: '/xnth/approval',
    method: 'post',
    data: data
  })
}
//  点击收藏
export function clickSc(data) {
  return request({
    url: '/xnth/reserve/clickSc?busiId='+data.busiId+'&busiType='+data.busiType,//1001
    method: 'get'
  })
}
// 部门字典查询
export function dictDept(query) {
  return request({
    url: '/xnth/util/dictDeptList',
    method: 'get',
    params: query
  })
}